# Mini tutorials on advanced programming skills

start date: 4 July 2019

## Contributors

- Malvika Sharan
- Alex Riera
- Ivica Lehotska

More contributions are welcome!

##### Project Summary

Bio-IT has been offering many beginners lessons on computing skills such as Python, R, Unix, git, etc. 
Our learners who finish these courses seek new and advanced skills that they can learn and apply in their work.
With this project idea, I would like to invite the bioinformaticians to share one of their favorite skills 
by creating a small tutorial that can be taught by someone, or can be used for self-paced training.

##### What do you hope to achieve during the Hackathon?

* [ ]  Short tutorial on skills for data analysis using techniques such as Machine Learning, statistical analysis, visualization, etc.
* [ ]  The tutorial should include the objective of the material, introduction to the concept, code snippets, examples, optional exercises and set of references that one can use to get more information.
* [ ]  The tutorial developed at the hackathon will be hosted on the git.embl.de using Open Source License.

##### What skills should people have to be able to contribute to the project?

* [ ]  Exeperience with one or multiple data science techniques or programming language.

##### Can you link to/provide information that contributors can/should read before the Hackathon?

- Optional: [What makes a good tutorial](https://www.makeuseof.com/tag/makes-good-programming-tutorial/)
- Possible format: [Carpentries Style Lesson](http://carpentries.github.io/lesson-example/)

##### Contribute to this project if you're interested in...

* [ ]  sharing a programming or computing skills.
* [ ]  Creating online learning resource.

##### Contact person/people

- Malvika Sharan (malvika.sharan@embl.de)

## License

[CC-BY-4.0](https://choosealicense.com/licenses/cc-by-4.0/) and [MIT](https://choosealicense.com/licenses/apache-2.0/)

