{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualizing MNIST hand written numbers with Tensorboard\n",
    "\n",
    "In this tutorial we will learn:\n",
    "\n",
    "1. How to use one of the PyTorch datasets\n",
    "    1. Download the MNIST dataset\n",
    "    1. Apply a series of transformations so it is ready to be used by our algorithm\n",
    "1. How to use the [`tensorboard`](https://www.tensorflow.org/guide/summaries_and_tensorboard) projection functionality to plot our dataset\n",
    "    1. Use `SummaryWriter` to create logs that `tensorboard` knows how to read\n",
    "    1. Some simple `torch` Tensor manipulation tricks\n",
    "    1. Get familiar with the `tensorboard` user interface"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Imports\n",
    "\n",
    "Make sure you have installed all the needed requirements to run this project. Please refer to the [README](README.md) file for details.\n",
    "\n",
    "We will be using 2 main pieces today:\n",
    "\n",
    "1. [PyTorch](https://pytorch.org/): a powerful deep learning framework. We will use its extension `torchvision` to download and pre-process existing datasets and basic Tensor operations\n",
    "2. [tensorboardX](https://github.com/lanpa/tensorboardX): a PyTorch-compatible port of Google's tensorboard (originally designed for tensorflow)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "from tensorboardX import SummaryWriter\n",
    "from torchvision import datasets, transforms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dataset: download and process\n",
    "\n",
    "PyTorch offers a list of datasets ready for download and use. Take a look at [the documentation](https://pytorch.org/docs/stable/torchvision/datasets.html)\n",
    "\n",
    "Today we will be using the MNIST dataset ([trochvision documentation](https://pytorch.org/docs/stable/torchvision/datasets.html#mnist), [wikipedia](https://en.wikipedia.org/wiki/MNIST_database)), which is simple set of images with hand written numbers commonly seen used in different image recognition tasks and tutorials.\n",
    "\n",
    "In the next step we will:\n",
    "\n",
    "1. download the dataset\n",
    "2. apply some basic transformations:\n",
    "    1. transfor the image pixels into a row of interger (tensor)\n",
    "    1. normalize every image to the median and standard deviation of the dataset (which we just happen to know are 0.13 and 0.38)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "transformations = transforms.Compose([\n",
    "    transforms.ToTensor(),\n",
    "    transforms.Normalize((0.1307,), (0.3081,))\n",
    "])\n",
    "mnist_trainset = datasets.MNIST(root='./data', train=True, download=True, transform=transformations)\n",
    "mnist_testset = datasets.MNIST(root='./data', train=False, download=True, transform=transformations)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Taking a look at the data\n",
    "If we inspect one individual example of the imported dataset:\n",
    "\n",
    "```\n",
    "tensor, label = mnist_testset[0]\n",
    "```\n",
    "\n",
    "we can see that it comes with (1) a tensor representing the image and (2) the label of that image, that is, the number that the image of the hand-written number represents.\n",
    "\n",
    "Inspecting the shape of our tensor we also see the dimentions are `torch.Size([1, 28, 28])`. As you can see it is a 3 dimentional tensor, in this case this means that our image:\n",
    "\n",
    "* is of size 28 pixels by 28 pixels\n",
    "* and has 1 channel (since it is a black and white image). We would have 3 channels in a full colour RGB image\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "7\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "torch.Size([1, 28, 28])"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tensor, label = mnist_testset[0]\n",
    "print(label)\n",
    "tensor.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we will learn how to reshape our 3 dimensional tensor to a simple 1 dimensional tensor. This will be useful later on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "torch.Size([784])\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "tensor([-0.4242, -0.4242, -0.4242, -0.4242, -0.4242, -0.4242, -0.4242, -0.4242,\n",
       "        -0.4242, -0.4242, -0.4242, -0.4242, -0.4242, -0.4242, -0.4242, -0.4242,\n",
       "        -0.4242, -0.4242, -0.4242, -0.4242])"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "vector = torch.reshape(tensor, (-1,))\n",
    "print(vector.shape)\n",
    "vector[:20]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create logs for Tensorboard\n",
    "\n",
    "Tensorboard as a funny way of working in which you dont access the raw data like you do in this jupyter notebook, but rather write to a log file (using their own API) and then later the tensorboard webserver will be able to read and interpret this logs.\n",
    "\n",
    "For the projection functionality of Tensorboard that we want to use, the one that will allow us to use T-SNE and PCA for visualization, we need to make use of the embeddings functionality, for that we will:\n",
    "\n",
    "1. convert every example into a 1 dimensional vector (as we did above)\n",
    "2. using the [`add_embedding`](https://tensorboardx.readthedocs.io/en/latest/tutorial.html#add-embedding) method of the tensorboard API, add this vector to the tensorboard log\n",
    "\n",
    "As a result a new `runs` directory will be created with the input that tensorboard is expecting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "number_of_examples = 1000\n",
    "examples = torch.Tensor(number_of_examples, 784)\n",
    "for i in range(number_of_examples):\n",
    "    tensor, label = mnist_testset[i]\n",
    "    examples[i] = torch.reshape(tensor, (-1,))\n",
    "writer = SummaryWriter(comment='mnist')\n",
    "writer.add_embedding(examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualizing with tensorboard\n",
    "\n",
    "Finally the momento of truth!\n",
    "Go to your console and launch a new tensorboard web server. Make sure to specify the path to your `runs` directory with the flag `--log-dir` , like this:\n",
    "\n",
    "```bash\n",
    "tensorboard --logdir=runs\n",
    "```\n",
    "\n",
    "With this you should be able to open your locally served tensorbaord web under http://127.0.0.1:6006/.\n",
    "\n",
    "Once in the website, go to the top right corner and, in the dropdown, select **projections**\n",
    "\n",
    "![](screenshots/tensorboard01.png)\n",
    "\n",
    "That should take you to a page that generates a visualzation like this one:\n",
    "\n",
    "![](screenshots/tensorboard02.png)\n",
    "\n",
    "Cool looking, but not very useful, since we dont really know which image is represented by which dot.\n",
    "\n",
    "Next, let's review the code above and add some extra metadata information, that will include:\n",
    "\n",
    "1. the original image\n",
    "2. the label (aka the number that the image actually is)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "number_of_examples = 1000\n",
    "examples = torch.Tensor(number_of_examples, 784)\n",
    "labels = []\n",
    "imgs = torch.Tensor(number_of_examples, 1, 28, 28)\n",
    "for i in range(number_of_examples):\n",
    "    tensor, label = mnist_testset[i]\n",
    "    examples[i] = torch.reshape(tensor, (-1,))\n",
    "    imgs[i] = tensor\n",
    "    labels.append(label)\n",
    "writer = SummaryWriter(comment='mnist_with_labels')\n",
    "writer.add_embedding(examples, metadata=labels, label_img=imgs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After you run the block above, inside your `runs` folder, there will be a new subfolder with the timestamp and the comment that we gave to the summary writer (`mnist_with_labels`).\n",
    "\n",
    "Now go to your tensorboard website again and refresh the page. In order to visualize your latest run look on the left hand side column for a dropdown that allows you to select your runs, and choose the latest one.\n",
    "\n",
    "![](screenshots/tensorboard03_.png)\n",
    "\n",
    "You can see now the original hand-written images, and if you click over one of them you will reveal the label for each example.\n",
    "\n",
    "At the bottom of the left-hand side column you have different dimensionality reduction algorithms with the different configuration parameter available. I have found that t-sne works particularly well with this dataset.\n",
    "\n",
    "Have fun playing with the different options :)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
