In this tutorial we will be (ab)using a pre-trained deep neural network for our advantage.
We start with an set (big or small) of images. Our goal is to visualize how do they relate to each other by plotting them in a 2D or 3D space.

# Tutorials

Divided in several Jupyter Notebooks, they will walk you through the basics and build on each others knowledge
* [`01. tensorboard and pytorch basics.ipynb`](./01. tensorboard and pytorch basics.ipynb)
    Will show you the basics on how to use pytorch and tensboard to create interactive visualizations
* [02]()
    Will get you through the steps on how to use a pre-trained neural network to generate an embedding that you will visualize using tensorboard again.


### Dependencies
The main players will be:

* `pytorch` for Neural Networks
* `tensorboard` for visualizations

Check the `requirements.txt` file for specific details.
### How to run the code

```bash
# Create and activate a new python virtual environment in Python 3 (you may use conda if you prefer)
python3 -v venv .venv
source .venv/bin/activate

# Make sure pip and setuptools are up-to-date
pip install --upgrade pip setuptools

# Install the dependencies
pip install -r requirements.txt

# launch Jupyter
jupyter notebook
```

### Authorship

Feedback, errors, comments, complains, compliments... I'd be very happy to hear all of that, just send me an email at [alejandro.riera@embo.org](mailto:alejandro.riera@embo.org)
