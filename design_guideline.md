# Template for mini-tutorial (suggested)

## Goal of the project: Build a collection of programming tutorial for advanced learners.

## The Lesson Design Proposal

We will use the backward design process, which has three stages:
- Identify the practical skills we aim to teach our learners
- Design challenges to give an opportunity for our learners to practice and integrate these skills
- Identify what we need to teach for our learners to acquire these skills


### PART 1: List out on the top of your document

- Development Team Members – Who are the people involved?
- The intended audience – Who is the target audience for this lesson? (e.g. Graduate-level researchers in biotechnology, bioinformatics, etc.).
- Duration of the lesson – How long is this lesson? (e.g. 1.5, 3, 6, or 12 hours)
- A short description – It should be ~5 sentences and capture the type of data and general audience for the lesson (e.g. use bullet points)
- Topics to be described in this section - list all the topic that will be discussed in this section
- Computation resource(s) used – Describe the software and/or database that will be taught in the lesson 
- Suggestions for the examples to use – Select a few examples that will be used throughout your lesson


### PART 2: A brief lesson outline

For each material, please describe:

- 3–6 concrete learning objectives.
- Expand on the topics
- An end-of-lesson assessment exercises to demonstrate the skills participants have learned.
- A summary of the tools and data set(s) that will be used.
- A point-form learning plan

***Hint: Create a Development Plan – Draw a flowchart/mental model of how the lesson will be designed (take a picture and add them in your document)***

------------------------------------------------------------------

### PART 3: Resources and Future plans

- Resources – Summarize top 5 useful resources that researchers need this lesson. This part may include links to online resources 
- Future plan – Explain who will support lesson development after today. What expertise will be needed, what resources are missing etc.
