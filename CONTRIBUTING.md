*Welcome! Very glad that you are planning to contribute to this repository!*

## About

This repository is a place to collect bite size tutorials on different cool stuff that we can do with programming.

Have you recently learned something that took you forever? Would you like to save others from dealing in the same problems that you have already solved? Then, this is the place for you.

All your contributions will be hosted under CC-By-4.0 and MIT license unless explicitely mentioned otherwise.

Please use [this guideline](https://git.embl.de/grp-bio-it/mini_tutorial/blob/master/design_guideline.md) to design your tutorial.

Get in touch with [Bio-IT](mailto:bio-it@embl.de) other contributors if you have any questions.